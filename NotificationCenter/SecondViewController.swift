//
//  SecondViewController.swift
//  NotificationCenter
//
//  Created by Amol Tamboli on 07/03/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnFb(_ sender: Any) {
        NotificationCenter.default.post(name: .Facebook, object: nil)
    }
    
    @IBAction func btnTwitter(_ sender: Any) {
        NotificationCenter.default.post(name: .Twitter, object: nil)
    }
}
