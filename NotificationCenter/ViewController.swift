//
//  ViewController.swift
//  NotificationCenter
//
//  Created by Amol Tamboli on 07/03/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(facebook(notification:)), name: .Facebook, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(twitter(notification:)), name: .Twitter, object: nil)

    }
    //MARK:- FACEBOOK
    @objc func facebook(notification:Notification){
        lbl.text = "Facebook"
        img.image = #imageLiteral(resourceName: "Facebook")
    }
    
    //MARK:- TWITTER
    @objc func twitter(notification:Notification){
        lbl.text = "Twitter"
        img.image = #imageLiteral(resourceName: "Twitter")
    }
    
    @IBAction func btnSelectClick(_ sender: Any) {
        let seconVC = self.storyboard?.instantiateViewController(identifier: "SecondViewController") as! SecondViewController
        self.navigationController?.pushViewController(seconVC, animated: true)
    }
    
}

extension Notification.Name {
    static let Facebook = Notification.Name ("Facebook")
    static let Twitter = Notification.Name ("Twitter")
}
